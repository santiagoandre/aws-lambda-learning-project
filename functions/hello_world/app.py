import json
import requests 
# from logic import layer_function
import logic # file inside logic_layer
import main  # file inside s3 layer
def s3_conection():
    name  = "testbucket"
    host = 'http://localstack:4566'

    s = main.S3File(host)
    files = s.files(name)
    file_name = 'test_image1.png'
    # upload
    # f = open('images/test_image.png','rb')
    # print(files.upload(file_name, f ))

    # download
    # n o funciona en una lambda ya que se monta sobre un sistema de solo lectura,
    # ademas no tiene sentido descargar una imagen en una layer
    # f = files.download(file_name,'images/download/'+file_name)
    # print(dir(f))
    # print(type(f))
    # print(f)

    # presigned url

    response = files.get_presigned_url(file_name,3600)
    print(response)
def lambda_handler(event, context):
    """Sample pure Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """
    logic.layer_function()
    s3_conection()
    return {
        "statusCode": 200,
        "body": json.dumps(
            {
                "message": "hello world",
            }
        ),
    }
