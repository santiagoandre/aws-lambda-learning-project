from abc import ABC, abstractmethod
import boto3
from botocore.config import Config

class BotoClient(ABC):    
    @abstractmethod
    def s3_aws_services_connection( self, endpoint):
        pass
    def get_configuration( self):
        return os.environ.get('s3_host')

class LocalyClient(BotoClient):
    def s3_aws_services_connection( self, endpoint):
        try:
            session = boto3.client( "s3", 
                                    config = Config(signature_version='s3v4'),
                                    endpoint_url = endpoint
                                 )
        except Exception as e:
            return "Error {}".format(e)
        else:
            return session 
