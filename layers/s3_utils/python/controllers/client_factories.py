from abc import ABC, abstractmethod
from .clients import LocalyClient
class ClientFactory(ABC):
    @abstractmethod
    def factory_method( self ):
        pass

class LocalClientFactory(ClientFactory):
    def factory_method(self):
        return LocalyClient()
        