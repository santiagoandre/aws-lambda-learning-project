from controllers import client_factories 
from controllers import files
LocalClientFactory = client_factories.LocalClientFactory
class S3File:
    def __init__( self ,endpoint_url):
        self.client = LocalClientFactory().factory_method().s3_aws_services_connection(endpoint_url) # agregar una clase que decida si crear un cliente real o local
    
    def files(self,bucket):
        documents = files.Files( self.client, bucket )
        return documents
    def documents(self):
        pass
    def images(self):
        pass



name  = "testbucket"
host = 'http://localhost:4566'

s = S3File(host)
files = s.files(name)
file_name = 'test_image.png'
# upload
# f = open('images/test_image.png','rb')
# files.upload(file_name, f )

# download

# f = files.download(file_name,'images/download/'+file_name)
# print(dir(f))
# print(type(f))
# print(f)

# presigned url

response = files.get_presigned_url(file_name,3600)
print(response)