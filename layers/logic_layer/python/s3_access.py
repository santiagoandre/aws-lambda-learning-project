import boto3
import os
from botocore.exceptions import ClientError






class Files:
    def __init__( self,bucket):
        
        self.bucket = bucket
        endpoint_url = os.environ.get('AWS_URL')
        if endpoint_url:
            self.s3_client = boto3.client('s3',endpoint_url =  endpoint_url)
        else:
            self.s3_client = = '' # production keys

    
 
    
    def download( self, file_name, file_patch ):
        try:
            result = self.s3_client.download_file( self.bucket, file_name, 
                                                    file_patch
                                                )
        except ClientError as e:
            return "Error {}".format(e)
        else:
            return "/tmp/{}".format( file_name )  

    
    def upload( self, file_name, file ):
        
        try:
            response = self.s3_client.put_object(  Body = file,
                                                   Bucket = self.bucket, 
                                                   Key = file_name
                                                )
        except (ClientError, FileNotFoundError) as e:
            return "Error {}".format(e)
        else:
            return response
    
    
    def get_presigned_url( self, file_name, expiration ):
        try:
            response = self.s3_client.\
                        generate_presigned_url( "get_object", 
                                                Params = { "Bucket" : self.bucket,
                                                            "Key" : file_name },
                                                ExpiresIn = expiration,
                                                HttpMethod = "https"
                                            )
        except ClientError as e:
            return "Error {}".format(e)
        else:
            return response


