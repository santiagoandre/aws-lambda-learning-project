import boto3

def create_s3_bucket(host,region,name):
    s3 = boto3.resource('s3', endpoint_url=host,region_name=region)
    s3.create_bucket(Bucket=name,CreateBucketConfiguration={
    'LocationConstraint': region})

# CREATE BUCKET
host = 'http://localhost:4566'
region = 'us-east-1'#'sa-east-1' #'us-east-1'
name  = "testbucket"
create_s3_bucket(host,region,name)



# UPLOAD IMAGE